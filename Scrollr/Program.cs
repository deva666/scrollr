﻿/* Copyright Marko Devcic, madevcic ( at ) gmail.com
 * License: Free, GPL v3.0 http://www.gnu.org/licenses/gpl-3.0.html
 */

using Microsoft.Win32;
using System;
using System.Threading;
using System.Windows.Forms;

namespace Scrollr
{
    static class Program
    {
        static Mutex mutex;
        static Scrollr scrollr;
        static TrayIcon trayIcon;
        [STAThread]
        static void Main()
        {
            mutex = new Mutex(true, Guid.NewGuid().ToString());

            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                scrollr = new Scrollr();
                trayIcon = new TrayIcon();
                Application.ApplicationExit += (s, e) =>
                {
                    scrollr.Dispose();                    
                    trayIcon.Dispose();                    
                };
                SystemEvents.SessionEnding += (s, e) => { Application.Exit(); };
                Application.Run(scrollr);
            }

        }
    }
}
