﻿/* Copyright Marko Devcic, madevcic ( at ) gmail.com
 * License: Free, GPL v3.0 http://www.gnu.org/licenses/gpl-3.0.html
 */

using Scrollr.Hooks;
using Scrollr.NativeMethods;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Scrollr
{
    class Scrollr : ApplicationContext, IDisposable
    {
        MouseHook _mouseHook;
   
        const uint SW_SHOW = 5;

        public Scrollr()
        {
            _mouseHook = new MouseHook();
            _mouseHook.MouseWheel += mouseHook_MouseWheel;
        }

        public void mouseHook_MouseWheel(object sender, MouseHookArgs e)
        {
            IntPtr hWnd = WinAPI.WindowFromPoint(e.Point);
            while (true)
            {
                IntPtr temp = WinAPI.GetParent(hWnd);
                if (temp.Equals(IntPtr.Zero)) break;
                hWnd = temp;
            }
            if (WinAPI.GetActiveWindow() != hWnd) ActivateWindow(hWnd);
        }

        private void ActivateWindow(IntPtr hWnd)
        {
            uint appThread = WinAPI.GetCurrentThreadId();
            uint foregroundThread = WinAPI.GetWindowThreadProcessId(WinAPI.GetForegroundWindow(), IntPtr.Zero);
            WinAPI.AttachThreadInput(foregroundThread, appThread, true);
            WinAPI.BringWindowToTop(hWnd);
            WinAPI.ShowWindow(hWnd, SW_SHOW);
            WinAPI.AttachThreadInput(foregroundThread, appThread, false);
        }

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            _mouseHook.Dispose();
            base.Dispose(disposing);            
        }

        public new void Dispose()
        {
            Dispose(true);            
        }

        #endregion
    }
}
