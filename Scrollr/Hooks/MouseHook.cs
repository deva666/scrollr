﻿/* Copyright Marko Devcic, madevcic ( at ) gmail.com
 * License: Free, GPL v3.0 http://www.gnu.org/licenses/gpl-3.0.html
 */

using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using Scrollr.NativeMethods;

namespace Scrollr.Hooks
{
    public enum MouseButton
    {
        LeftDown,
        RightDown,
        LeftUp,
        RightUp,
        Wheel,
        None
    }

    sealed class MouseHook : IDisposable
    {
        #region Fields

        public const int WM_LBUTTONDOWN = 0x0201;
        public const int WM_RBUTTONDOWN = 0x0204;
        public const int WM_LBUTTONUP = 0x0202;
        public const int WM_RBUTTONUP = 0x0205;
        public const int WH_MOUSE_LL = 14;
        public const int WM_MOUSEWHEEL = 0x020A;

        Point point;
        WinAPI.MSLLHOOKSTRUCT mouseStruct;

        public event EventHandler<MouseHookArgs> MouseMove;
        public event EventHandler<MouseHookArgs> MouseClickUp;
        public event EventHandler<MouseHookArgs> MouseClickDown;
        public event EventHandler<MouseHookArgs> MouseWheel;

        public delegate IntPtr HookHandlerCallBack( int code, IntPtr wParam, IntPtr lParam );

        public HookHandlerCallBack hookCallBack;
        IntPtr hookID = IntPtr.Zero;

        #endregion

        #region Constructor

        public MouseHook( )
        {
            point = new Point( );
            hookCallBack = new HookHandlerCallBack( HookProc );
            using ( Process process = Process.GetCurrentProcess( ) )
            {
                using ( ProcessModule module = process.MainModule )
                {
                    hookID = Win32.SetWindowsHookEx( WH_MOUSE_LL, hookCallBack, WinAPI.GetModuleHandle( module.ModuleName ), 0 );
                }
            }
        }

        #endregion

        #region HookCallBack

        IntPtr HookProc( int code, IntPtr wParam, IntPtr lParam )
        {
            if ( code < 0 )
            {
                return WinAPI.CallNextHookEx( IntPtr.Zero, code, wParam, lParam );
            }
            mouseStruct = ( WinAPI.MSLLHOOKSTRUCT ) Marshal.PtrToStructure( lParam, typeof( WinAPI.MSLLHOOKSTRUCT ) );

            if ( point != mouseStruct.pt )
            {
                if ( MouseMove != null )
                {
                    MouseMove( this, new MouseHookArgs( mouseStruct.pt, ( int ) wParam ) );
                }
            }
            point = mouseStruct.pt;

            if ( wParam == ( IntPtr ) WM_MOUSEWHEEL )
            {
                bool scrollUp;
                if ( mouseStruct.mouseData > 0 )
                    scrollUp = true;
                else
                    scrollUp = false;
                if ( MouseWheel != null )
                    MouseWheel( this, new MouseHookArgs( point, ( int ) wParam, scrollUp ) );
            }

            if ( wParam == ( IntPtr ) WM_LBUTTONDOWN || wParam == ( IntPtr ) WM_RBUTTONDOWN )
            {
                if ( MouseClickDown != null )
                {
                    MouseClickDown( this, new MouseHookArgs( mouseStruct.pt, ( int ) wParam ) );
                }
            }

            if ( wParam == ( IntPtr ) WM_LBUTTONUP || wParam == ( IntPtr ) WM_RBUTTONUP )
            {
                //do nothing
            }

            return WinAPI.CallNextHookEx( hookID, code, wParam, lParam );
        }

        #endregion

        internal class Win32
        {
            [DllImport( "user32.dll", CharSet = CharSet.Auto, SetLastError = true )]
            public static extern IntPtr SetWindowsHookEx( int idHook, HookHandlerCallBack lpfn, IntPtr hMod, uint dwThreadId );
        }

        #region IDisposable Members

        ~MouseHook( )
        {
            Dispose( false );
#if DEBUG
            Debug.WriteLine("FINALIZER CALLED");
#endif
        }

        private void Dispose( bool disposing )
        {
            WinAPI.UnhookWindowsHookEx( hookID );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion
    }

    public class MouseHookArgs : EventArgs
    {
        Point _point;
        int _mouseButton;

        public Point Point { get { return _point; } }

        public bool ScrollUp { get; set; }
        public MouseButton MouseButton
        {
            get
            {
                if ( _mouseButton == MouseHook.WM_LBUTTONDOWN )
                {
                    return MouseButton.LeftDown;
                }
                else if ( _mouseButton == MouseHook.WM_RBUTTONDOWN )
                {
                    return MouseButton.RightDown;
                }
                else if ( _mouseButton == MouseHook.WM_LBUTTONUP )
                {
                    return MouseButton.LeftUp;
                }
                else if ( _mouseButton == MouseHook.WM_RBUTTONUP )
                {
                    return MouseButton.RightUp;
                }
                else
                    return MouseButton.None;
            }
        }

        public MouseHookArgs( Point point, int mouseButton )
        {
            _point = point;
            _mouseButton = mouseButton;
        }

        public MouseHookArgs( Point point, int mouseButton, bool scrollUp )
            : this( point, mouseButton )
        {
            this.ScrollUp = scrollUp;
        }

    }
}
