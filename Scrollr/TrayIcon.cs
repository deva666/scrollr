﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Scrollr
{
    sealed class TrayIcon : IDisposable
    {
        bool _disposed = false;
        NotifyIcon _notifyIcon;
        ContextMenuStrip _iconMenu;

        ToolStripMenuItem _menuItemExit;

        public bool IsVisible { get { return _notifyIcon.Visible; } set { _notifyIcon.Visible = value; } }

        #region Constructor

        public TrayIcon( )
        {
            _notifyIcon = new NotifyIcon( );
            _notifyIcon.Text = "Scrollr";
            _iconMenu = new ContextMenuStrip( );
            _menuItemExit = new ToolStripMenuItem( "Exit" );
            _iconMenu.Items.Add( _menuItemExit );
            _notifyIcon.ContextMenuStrip = _iconMenu;
            _notifyIcon.Icon = Properties.Resources._1391983939_36439;
            _notifyIcon.Visible = true;

            #region Event Handlers

            _menuItemExit.Click += ( s, e ) => Application.Exit( );

            #endregion
        }

        #endregion


        #region IDisposable Members

        public void Dispose( )
        {
            Dispose( true );
        }

        private void Dispose( bool disposing )
        {
            if ( disposing )
            {
                if ( _disposed )
                    return;                
                _menuItemExit.Dispose( );
                _iconMenu.Dispose( );
                _notifyIcon.Dispose( );
                _disposed = true;
            }
        }

        #endregion
    }
}
